extends Node2D

# Some initial points
var points = [Vector2(100, 100), Vector2(500, 100)]

var path_follow
var icon_ref
var speed = 100
var curve
var points_to_draw = []

func _ready():
	var path = Path2D.new()
	curve = Curve2D.new()
	for point in points:
		curve.add_point(point)
	path.set_curve(curve)
	
	call_deferred("add_child", path)
	path_follow = PathFollow2D.new()
	path_follow.loop = false
	path.call_deferred("add_child", path_follow)
	icon_ref = $icon # this is short form of get_node("icon")
	icon_ref.position = points[0]

func _input(event):
	if event.is_action_pressed('my_mouse_left_click'):
		var current_pos = icon_ref.position
		var mouse_pos = event.position
		
		curve.clear_points()
		curve.add_point(current_pos)
		curve.add_point(mouse_pos)
		
		points.clear()
		points.append(current_pos)
		points.append(mouse_pos)
		
		path_follow.offset = 0

func _draw():
	for idx in points.size() - 1:
		draw_line(points[idx], points[idx + 1], Color(.6, .6, .6))
	for point in points:
		draw_circle(point, 3, Color(.9, .3, .3))

func _process(delta):
	path_follow.offset += speed * delta
	icon_ref.position = path_follow.position
	icon_ref.rotation = path_follow.rotation
	icon_ref.rotate(deg2rad(270))
	update()
